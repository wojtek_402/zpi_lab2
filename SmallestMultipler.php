<?php
    /*
     * @author Wojciech Fajczyk
     * Refactorized by Wojciech Król (26-04-2015)
     */
    
    class SmallestMultipler
    {
        /**
         * Contains the smallest multiple number calculated by thic class
         * @access private
         * @var MinNumber int
         */
        private $MinNumber = 0;
        
        /**
         * Contains the begin of a set.
         * @access private
         * @var NumberFrom int
         */
        private $NumberFrom = null;
        
        /**
         * Contains the end of a set.
         * @access private
         * @var NumberTo int
         */
        private $NumberTo = null;

        /**
         * Output text.
         * @access private
         * @var output string
         */
        private $Output;
        
        
        /**
         * Initializing begin and end of set.
         * @access public
         * @param int $from Begin
         * @param int $to End
         * 
         * Example:
         * $SmallNumber = new SmallestMultipler(1,10);
         */
        public function __construct($from, $to)
        {
            $this->NumberFrom = $from;
            $this->NumberTo = $to;
        }

        /**
         * Displays output text.
         * @access public
         * @return string
         *
         * Example:
         * echo $SmallNumber->OutputText();
         *
         * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
         * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
         * Answer: 232792560
         *
         */
        public function OutputText()
        {
            $this->Output = '2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.<br/>';
            $this->Output .= 'What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20? <br/> <br/>';
            if($this->MinNumber)
            {
                $this->Output .= '<strong>Answer: '.$this->MinNumber.' </strong>';
            } else {
                $this->Output .= '<strong>Answer: '.(string)$this->SearchMultipler().' </strong>';
            }

            return $this->Output;
        }
        
        /**
         * Calculating smallest multipler.
         * @access public
         * @function SearchMultipler
         * @return int/bool
         *
         * For set from 1 to 20
         * Example:
         * echo $SmallNumber->SearchMultipler();
         *
         * 232792560
         */
        public function SearchMultipler()
        {
            //counter
            $i=1;
            $dividable = false;

            //loop who search value
            while($dividable==false)
            {
                $dividable = true;
                for ($j = $this->NumberTo; $j > $this->NumberFrom; $j--)
                {
                    if ($i % $j != 0)
                    {
                        $dividable = false;
                        break;
                    }

                }
                if ($dividable == true)
                {
                    $this->MinNumber =$i;
                    return $this->MinNumber;
                }
                $i+=1;
            }
            return false;
        }
    }
?>
