<?php

    class SmallestMultiplerTest extends PHPUnit_Framework_TestCase
    {
        
        public function testFirstSmallestMultipler()
        {
            $object = new SmallestMultipler(1,10);
            
            $a = $object->SearchMultipler();
            $b = 2520;
            
            $this->assertEquals($b, $a);
        }
		
		public function testSecondSmallestMultipler()
		{
			$object = new SmallestMultipler(1,20);
			
			$a = $object->SearchMultipler();
			$b = 232792560;
			
			$this->assertEquals($b, $a);
		}
    }
    
?>